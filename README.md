
# Flutter Repo Viewer

  

  

## Objectives

  

  

The objective of this project is to develop a robust and feature-rich Flutter application that showcases the most starred and update time sorted GitHub repositories related to Flutter . The application is designed with the following goals in mind:

  

  

1. **Demonstrate Best Practices:** Create a codebase that adheres to industry best practices and follows clean code principles. Emphasize readability, maintainability, and reusability of the code.

  

  

2. **Scalability:** Build the application with scalability in mind, considering future feature enhancements and the ability to handle larger datasets. Use appropriate architectural patterns to support the growth of the project.

  

  

3. **Offline Mode:** Implement offline functionality by storing fetched repository data in a local database. This allows users to access and browse repositories even when they are not connected to the internet.

  

  

4. **Efficient Data Retrieval:** Fetch repository list from the GitHub API using the keyword "Flutter" as the query. Implement pagination to load new items as the user scrolls, enhancing the user experience and optimizing network usage.

  

  

5. **Data Refreshing:** Control the frequency of data refreshing from the API to minimize network calls. Ensure that the repository data is refreshed no more frequently than once every 30 minutes.

  

  

6. **Sorting Options:** Provide users with the ability to sort the repository list based on either the last updated date-time or the star count. Persist the selected sorting option across app sessions for a personalized experience.

  

  

7. **User Interface:** Design an intuitive and visually appealing user interface. Include a repository list view, a sorting icon for quick access to sorting options, and a repository details page with clear and concise information.

  

  

## Design philosophy

  

  

- In my project, I have chosen Riverpod as the state management solution. Riverpod has a smell like Bloc, but what attracted me the most is its StateNotifier feature. It allows me to easily manage and update the application state in a reactive manner.

  

  

- To maintain a single codebase with customizable variables such as base URLs or Firebase configurations, I have implemented three flavors for this project: "prod" for production, "stag" for staging, and "dev" for development. I utilized a build configuration file to handle these flavors effectively. This approach enables me to switch between different configurations without modifying the code. Although I'm currently using a single base URL, having this structure in place ensures scalability and flexibility for future changes.

  

  

- For local data storage, I have opted for Hive. I store the local data as a `Map<pageNumber, List<Repository>>` to reduce searching complexity. This approach allows me to access specific lists of repositories based on the page number. Additionally, I have optimized the storage of repository details. Since I retrieve repository details from the list, there is no need to duplicate already saved data, making API calls more efficient.

  

  

```

  

├── lib

  

│ ├── app

  

│ │ ├── application

  

│ │ │ ├── app.dart

  

│ │ │ ├── home_page_notifier

  

│ │ │ │ ├── home_page_notifier.dart

  

│ │ │ │ └── home_page_notifier_state.dart

  

│ │ │ ├── search_sort_word_notifier

  

│ │ │ │ ├── search_sort_word_notifier.dart

  

│ │ │ │ └── search_sort_word_notifier_state.dart

  

│ │ │ └── view

  

│ │ │ └── app.dart

  

│ │ ├── core

  

│ │ │ ├── error

  

│ │ │ │ └── failure.dart

  

│ │ │ ├── extensions

  

│ │ │ │ └── date_time_converter_extension.dart

  

│ │ │ ├── route

  

│ │ │ │ ├── error_route.dart

  

│ │ │ │ ├── route_provider.dart

  

│ │ │ │ └── routes.dart

  

│ │ │ ├── utils

  

│ │ │ │ └── toaster.dart

  

│ │ │ ├── values

  

│ │ │ │ └── asset_url.dart

  

│ │ │ └── widgets

  

│ │ │ ├── custom_text.dart

  

│ │ │ ├── network_image_handler.dart

  

│ │ │ └── retry_page.dart

  

│ │ ├── domain

  

│ │ │ └── data

  

│ │ │ ├── api

  

│ │ │ ├── data_domain

  

│ │ │ └── data_source

  

│ │ ├── infrastructure

  

│ │ │ ├── api_client_i

  

│ │ │ │ └── api_client_i.dart

  

│ │ │ ├── data

  

│ │ │ │ ├── api_data_source_i

  

│ │ │ │ └── local_data_source_i

  

│ │ │ └── data_domain_i

  

│ │ │ └── data_domain_i.dart

  

│ │ └── presentation

  

│ │ ├── home_page

  

│ │ │ ├── home_page.dart

  

│ │ │ └── widget

  

│ │ └── repository_details_page

  

│ │ ├── repository_details_page.dart

  

│ │ └── widgets

  

│ ├── core

  

│ │ └── values

  

│ ├── di

  

│ │ └── dependency_injector.dart

  

│ ├── flavor

  

│ │ ├── build_config.dart

  

│ │ ├── env_config.dart

  

│ │ └── enviroment.dart

  

│ ├── l10n

  

│ │ ├── arb

  

│ │ │ ├── app_en.arb

  

│ │ │ └── app_es.arb

  

│ │ └── l10n.dart

  

│ ├── main_development.dart

  

│ ├── main_production.dart

  

│ └── main_staging.dart

  

```

  

  

- I have structured my project into four layers: application, domain, infrastructure AKA implementation, and presentation.

  

- application part contain all the notifiers and related states.

  

- presentation part contain all the view and user interaction interact from the presentation to the application layer.

  

- domain layer is an abstraction of all the logical operation.

  

- infrastructure layer is implementation part of the domain layer. and they are both coupled via dependency injection.

  

  

![Flow chart](https://gitlab.com/xahid/flutter_repo_viewer/-/raw/main/media/flow_chart.png)

  

  

- I have split data layer into 3 pieces.

  

- (1) is api client, top layer, which connect to directly to the git api, it return either successful response means list or failure.

  

  

- (2) middle point, called dataSource layer data source layer is split in two different part local and api/remote. local data source return data if there is data available, else it will call the apidatasource, get the response, if the response is ok, then it saved the data and return the data, if failure, then return failure.

  

  

- (3) last and lowest layer is datadomain layer, which get data from the data source layer. which seems extra for now, I can easily use localData source as bottom layer. But I like to have another layer for fixing one exchange point. the application layer's home notifier use this bottom layer for response of the user interaction and make states.

  

  

## Screenshots

  

  

![ss](https://gitlab.com/xahid/flutter_repo_viewer/-/raw/main/media/Screenshot_1687602904.png)![ss](https://gitlab.com/xahid/flutter_repo_viewer/-/raw/main/media/Screenshot_1687603256.png)![ss](https://gitlab.com/xahid/flutter_repo_viewer/-/raw/main/media/Screenshot_1687602943.png)

  

  

---

  

  

## Getting Started 🚀

  

  

This project contains 3 flavors:

  

  

- development

  

- staging

  

- production

  

  

To run the desired flavor either use the launch configuration in VSCode/Android Studio or use the following commands:

  

  

```sh

  

# Development

  

$ flutter run --flavor development --target lib/main_development.dart

  

  

# Staging

  

$ flutter run --flavor staging --target lib/main_staging.dart

  

  

# Production

  

$ flutter run --flavor production --target lib/main_production.dart

  

```

  

  

_\*Flutter Repo Viewer works on iOS, Android, Web, and Windows._

  

  

---

  

  

---

  

  

## Working with Translations 🌐

  

  

This project relies on [flutter_localizations][flutter_localizations_link] and follows the [official internationalization guide for Flutter][internationalization_link].

  

  

### Adding Strings

  

  

1. To add a new localizable string, open the `app_en.arb` file at `lib/l10n/arb/app_en.arb`.

  

  

```arb

  

{

  

"@@locale": "en",

  

"counterAppBarTitle": "Counter",

  

"@counterAppBarTitle": {

  

"description": "Text shown in the AppBar of the Counter Page"

  

}

  

}

  

```

  

  

2. Then add a new key/value and description

  

  

```arb

  

{

  

"@@locale": "en",

  

"counterAppBarTitle": "Counter",

  

"@counterAppBarTitle": {

  

"description": "Text shown in the AppBar of the Counter Page"

  

},

  

"helloWorld": "Hello World",

  

"@helloWorld": {

  

"description": "Hello World Text"

  

}

  

}

  

```

  

  

3. Use the new string

  

  

```dart

  

import  'package:flutter_repo_viewer/l10n/l10n.dart';

  

  

@override

  

Widget build(BuildContext context) {

  

final l10n = context.l10n;

  

return Text(l10n.helloWorld);

  

}

  

```

  

  

### Adding Supported Locales

  

  

Update the `CFBundleLocalizations` array in the `Info.plist` at `ios/Runner/Info.plist` to include the new locale.

  

  

```xml

  

...

  

  

<key>CFBundleLocalizations</key>

  

<array>

  

<string>en</string>

  

<string>es</string>

  

</array>

  

  

...

  

```

  

  

### Adding Translations

  

  

1. For each supported locale, add a new ARB file in `lib/l10n/arb`.

  

  

```

  

├── l10n

  

│ ├── arb

  

│ │ ├── app_en.arb

  

│ │ └── app_es.arb

  

```

  

  

2. Add the translated strings to each `.arb` file:

  

  

`app_en.arb`

  

  

```arb

  

{

  

"@@locale": "en",

  

"counterAppBarTitle": "Counter",

  

"@counterAppBarTitle": {

  

"description": "Text shown in the AppBar of the Counter Page"

  

}

  

}

  

```

  

  

`app_es.arb`

  

  

```arb

  

{

  

"@@locale": "es",

  

"counterAppBarTitle": "Contador",

  

"@counterAppBarTitle": {

  

"description": "Texto mostrado en la AppBar de la página del contador"

  

}

  

}

  

```

  
  

### What Remaining?

  

- [ ] Full unit testing coverage [Only data layer done.]

- [ ] UI Testing coverage.

### Future scope

  

- [ ] Redirecting to the Repository.

- [ ] Giving star from this app.
