import 'package:flutter_repo_viewer/app/domain/data/api/api_client.dart';
import 'package:flutter_repo_viewer/app/infrastructure/api_client_i/api_client_i.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';

void main() {
  late GetIt sl;

  setUp(() {
    sl = GetIt.instance;
    sl.reset();
  });

  test('should register ApiClientImplementation as ApiClient', () {
    sl.registerLazySingleton<ApiClient>(() => ApiClientImplementation());
    expect(sl<ApiClient>(), isA<ApiClientImplementation>());
  });
}
