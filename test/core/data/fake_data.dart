// ignore: non_constant_identifier_names
final FAKE_DATA = {
  "total_count": 31496,
  "incomplete_results": false,
  "items": [
    {
      "id": 46629305,
      "node_id": "MDEwOlJlcG9zaXRvcnk0NjYyOTMwNQ==",
      "name": "hacker-scripts",
      "full_name": "NARKOZ/hacker-scripts",
      "private": false,
      "owner": {
        "login": "NARKOZ",
        "id": 253398,
        "node_id": "MDQ6VXNlcjI1MzM5OA==",
        "avatar_url": "https://avatars.githubusercontent.com/u/253398?v=4",
        "gravatar_id": "",
        "url": "https://api.github.com/users/NARKOZ",
        "html_url": "https://github.com/NARKOZ",
        "followers_url": "https://api.github.com/users/NARKOZ/followers",
        "following_url":
            "https://api.github.com/users/NARKOZ/following{/other_user}",
        "gists_url": "https://api.github.com/users/NARKOZ/gists{/gist_id}",
        "starred_url":
            "https://api.github.com/users/NARKOZ/starred{/owner}{/repo}",
        "subscriptions_url":
            "https://api.github.com/users/NARKOZ/subscriptions",
        "organizations_url": "https://api.github.com/users/NARKOZ/orgs",
        "repos_url": "https://api.github.com/users/NARKOZ/repos",
        "events_url": "https://api.github.com/users/NARKOZ/events{/privacy}",
        "received_events_url":
            "https://api.github.com/users/NARKOZ/received_events",
        "type": "User",
        "site_admin": false
      },
      "html_url": "https://github.com/NARKOZ/hacker-scripts",
      "description": "Based on a true story",
      "fork": false,
      "url": "https://api.github.com/repos/NARKOZ/hacker-scripts",
      "forks_url": "https://api.github.com/repos/NARKOZ/hacker-scripts/forks",
      "keys_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/keys{/key_id}",
      "collaborators_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/collaborators{/collaborator}",
      "teams_url": "https://api.github.com/repos/NARKOZ/hacker-scripts/teams",
      "hooks_url": "https://api.github.com/repos/NARKOZ/hacker-scripts/hooks",
      "issue_events_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/issues/events{/number}",
      "events_url": "https://api.github.com/repos/NARKOZ/hacker-scripts/events",
      "assignees_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/assignees{/user}",
      "branches_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/branches{/branch}",
      "tags_url": "https://api.github.com/repos/NARKOZ/hacker-scripts/tags",
      "blobs_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/git/blobs{/sha}",
      "git_tags_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/git/tags{/sha}",
      "git_refs_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/git/refs{/sha}",
      "trees_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/git/trees{/sha}",
      "statuses_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/statuses/{sha}",
      "languages_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/languages",
      "stargazers_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/stargazers",
      "contributors_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/contributors",
      "subscribers_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/subscribers",
      "subscription_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/subscription",
      "commits_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/commits{/sha}",
      "git_commits_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/git/commits{/sha}",
      "comments_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/comments{/number}",
      "issue_comment_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/issues/comments{/number}",
      "contents_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/contents/{+path}",
      "compare_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/compare/{base}...{head}",
      "merges_url": "https://api.github.com/repos/NARKOZ/hacker-scripts/merges",
      "archive_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/{archive_format}{/ref}",
      "downloads_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/downloads",
      "issues_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/issues{/number}",
      "pulls_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/pulls{/number}",
      "milestones_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/milestones{/number}",
      "notifications_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/notifications{?since,all,participating}",
      "labels_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/labels{/name}",
      "releases_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/releases{/id}",
      "deployments_url":
          "https://api.github.com/repos/NARKOZ/hacker-scripts/deployments",
      "created_at": "2015-11-21T19:05:09Z",
      "updated_at": "2023-06-21T07:50:29Z",
      "pushed_at": "2023-04-10T19:20:26Z",
      "git_url": "git://github.com/NARKOZ/hacker-scripts.git",
      "ssh_url": "git@github.com:NARKOZ/hacker-scripts.git",
      "clone_url": "https://github.com/NARKOZ/hacker-scripts.git",
      "svn_url": "https://github.com/NARKOZ/hacker-scripts",
      "homepage": "",
      "size": 105,
      "stargazers_count": 46578,
      "watchers_count": 46578,
      "language": "JavaScript",
      "has_issues": true,
      "has_projects": false,
      "has_downloads": true,
      "has_wiki": false,
      "has_pages": false,
      "has_discussions": false,
      "forks_count": 6752,
      "mirror_url": null,
      "archived": false,
      "disabled": false,
      "open_issues_count": 68,
      "license": null,
      "allow_forking": true,
      "is_template": false,
      "web_commit_signoff_required": false,
      "topics": [],
      "visibility": "public",
      "forks": 6752,
      "open_issues": 68,
      "watchers": 46578,
      "default_branch": "master",
      "score": 1.0
    }
  ]
};
