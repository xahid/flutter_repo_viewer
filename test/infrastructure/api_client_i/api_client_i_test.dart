import 'package:dartz/dartz.dart';
import 'package:flutter_repo_viewer/app/core/error/failure.dart';
import 'package:flutter_repo_viewer/app/domain/data/api/api_client.dart';
import 'package:flutter_repo_viewer/app/infrastructure/api_client_i/api_client_i.dart';
import 'package:flutter_repo_viewer/app/infrastructure/data/local_data_source_i/repo_res_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';

import '../../core/data/fake_data.dart';

class MockApiClientInfrastructureLeft extends Mock
    implements ApiClientImplementation {
  @override
  Future<Either<Failure, GitRepoResponse>> getRepoResponse(
      {required Map<String, dynamic> body}) async {
    return Left(Failure(message: ""));
  }
}

class MockApiClientInfrastructureRight extends Mock
    implements ApiClientImplementation {
  @override
  Future<Either<Failure, GitRepoResponse>> getRepoResponse(
      {required Map<String, dynamic> body}) async {
    return Right(GitRepoResponse.fromJson(FAKE_DATA));
  }
}

void main() {
  late GetIt sl;

  setUp(() {
    sl = GetIt.instance;
    sl.reset();
  });

  test('Api Client Infrastructure False Check', () async {
    sl.registerLazySingleton<ApiClient>(
        () => MockApiClientInfrastructureLeft());
    expect(sl<ApiClient>(), isA<ApiClientImplementation>());

    final Either<Failure, GitRepoResponse> result =
        await sl<ApiClient>().getRepoResponse(body: {});

    print(result.runtimeType);

    expect(result, isA<Either<Failure, GitRepoResponse>>());
  });

  test('Api Client Infrastructure Right Check', () async {
    sl.registerLazySingleton<ApiClient>(
        () => MockApiClientInfrastructureRight());
    expect(sl<ApiClient>(), isA<ApiClientImplementation>());

    final Either<Failure, GitRepoResponse> result =
        await sl<ApiClient>().getRepoResponse(body: {});

    print(result.runtimeType);

    expect(result, isA<Either<Failure, GitRepoResponse>>());
  });
}
