import 'package:flutter_repo_viewer/app/infrastructure/data/local_data_source_i/repo_res_model.dart';
import 'package:flutter_test/flutter_test.dart';
import '../../core/data/fake_data.dart';

void main() {
  group('Model Classes', () {
    test('Repo Response Model. Should convert repoResponse from JSON', () {
      final repoResponse = GitRepoResponse.fromJson(FAKE_DATA);

      expect(repoResponse.totalCount, 10);
      expect(repoResponse.incompleteResults, false);
      expect(repoResponse.items, isA<List<GitRepoItem>>());
      expect(repoResponse.items!.length, 1);
      expect(repoResponse.items![0].id, 1);
      expect(repoResponse.items![0].name, 'repo1');
    });
  });
}
