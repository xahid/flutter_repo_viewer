import 'package:flutter/material.dart';
import 'package:flutter_repo_viewer/app/infrastructure/data/local_data_source_i/repo_res_model.dart';
import 'package:flutter_repo_viewer/app/presentation/repository_details_page/widgets/description_card/description_card.dart';
import 'package:flutter_repo_viewer/app/presentation/repository_details_page/widgets/short_view_card/short_view_card.dart';
import 'package:gap/gap.dart';

class RepositoryDetailsPage extends StatelessWidget {
  static String get path => "repoDetailsPage";
  static String get name => "repoDetailsPage";
  final GitRepoItem repoItem;
  const RepositoryDetailsPage({super.key, required this.repoItem});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Color(0xFF151B22),
          elevation: 0,
        ),
        backgroundColor: Color(0xFF151B22),
        body: Container(
          child: Column(
            children: [
              ShortViewCard(
                repoName: repoItem.name ?? "",
                ownerAvatar: repoItem.owner?.avatarUrl ?? "",
                ownerName: repoItem.owner?.login ?? "",
                starEarned: repoItem.stargazersCount.toString(),
                lastUpdated: repoItem.createdAt ?? "",
                totalFork: repoItem.forksCount.toString(),
                totalWatch: repoItem.watchersCount.toString(),
                score: repoItem.score.toString(),
              ),
              Gap(8),
              DescriptionCard(
                details: repoItem.description ?? "",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
