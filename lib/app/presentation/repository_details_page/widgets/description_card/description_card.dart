import 'package:flutter/material.dart';
import 'package:flutter_repo_viewer/app/core/widgets/custom_text.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';

class DescriptionCard extends StatelessWidget {
  final String details;
  const DescriptionCard({super.key, required this.details});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(20.r),
          ),
          border: Border.all(color: Colors.white.withOpacity(0.1))),
      // padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 16.w),
      margin: EdgeInsets.symmetric(horizontal: 8.w, vertical: 16.w),
      width: double.infinity,
      child: Column(
        children: [
          Gap(6),
          CustomText(
            "Details",
            fontSize: 24.sp,
          ),
          Divider(color: Colors.white.withOpacity(0.1)),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.w),
            child: CustomText(
              details,
              fontSize: 16.sp,
            ),
          ),
          Gap(16),
        ],
      ),
    );
  }
}
