import 'package:flutter/material.dart';
import 'package:flutter_repo_viewer/app/core/extensions/date_time_converter_extension.dart';
import 'package:flutter_repo_viewer/app/core/widgets/custom_text.dart';
import 'package:flutter_repo_viewer/app/core/widgets/network_image_handler.dart';
import 'package:flutter_repo_viewer/app/presentation/repository_details_page/widgets/title_with_icon_row/title_with_icon_row.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';

class ShortViewCard extends StatelessWidget {
  final String repoName;
  final String ownerName;
  final String ownerAvatar;
  final String starEarned;
  final String? lastUpdated;
  final String totalFork;
  final String totalWatch;
  final String score;

  const ShortViewCard({
    super.key,
    required this.repoName,
    required this.ownerAvatar,
    required this.ownerName,
    required this.starEarned,
    required this.lastUpdated,
    required this.totalFork,
    required this.totalWatch,
    required this.score,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(20.r),
          ),
          border: Border.all(color: Colors.white.withOpacity(0.1))),
      padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 16.w),
      margin: EdgeInsets.symmetric(horizontal: 8.w, vertical: 16.w),
      child: Column(
        children: [
          Container(
            height: 120.h,
            child: NetworkImageHandler(
              networkImageUrl: ownerAvatar,
              borderRadius: 120.r,
            ),
          ),
          Gap(16),
          CustomText(
            repoName,
            fontSize: 32.sp,
          ),
          Gap(16),
          TitleWithIconRow(
            title: "Owner",
            titleValue: ownerName,
            iconData: Icons.person,
            isBigOne: true,
          ),
          TitleWithIconRow(
            title: "Star Earned",
            titleValue: starEarned,
            iconData: Icons.star,
            isBigOne: true,
          ),
          TitleWithIconRow(
            title: 'Last Updated',
            titleValue: lastUpdated?.formatDateString() ?? "",
            iconData: Icons.person,
            isBigOne: true,
          ),
          Gap(12),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 0.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TitleWithIconRow(
                  iconData: Icons.fork_right_sharp,
                  title: "Forked",
                  titleValue: totalFork,
                ),
                TitleWithIconRow(
                  iconData: Icons.remove_red_eye_outlined,
                  title: "Watcher",
                  titleValue: totalWatch,
                ),
                TitleWithIconRow(
                  iconData: Icons.sports_score,
                  title: "Score",
                  titleValue: score,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
