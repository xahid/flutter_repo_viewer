import 'package:flutter/material.dart';
import 'package:flutter_repo_viewer/app/core/widgets/custom_text.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TitleWithIconRow extends StatelessWidget {
  final String title;
  final String titleValue;
  final IconData iconData;
  final bool isBigOne;

  const TitleWithIconRow({
    super.key,
    required this.title,
    required this.titleValue,
    required this.iconData,
    this.isBigOne = false,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          iconData,
          color: Colors.white.withOpacity(0.4),
          size: isBigOne ? 32.sp : null,
        ),
        CustomText(
          '$title : ',
          fontColor: Colors.white.withOpacity(0.4),
          fontSize: isBigOne ? 20.sp : null,
        ),
        CustomText(
          titleValue,
          fontColor: Colors.white,
          fontWeight: FontWeight.bold,
          fontSize: isBigOne ? 20.sp : null,
        ),
      ],
    );
  }
}
