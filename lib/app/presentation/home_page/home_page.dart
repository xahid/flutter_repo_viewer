import 'package:flutter/material.dart';
import 'package:flutter_repo_viewer/app/application/search_sort_word_notifier/search_sort_word_notifier_state.dart';
import 'package:flutter_repo_viewer/app/core/widgets/custom_text.dart';
import 'package:flutter_repo_viewer/app/core/widgets/retry_page.dart';
import 'package:flutter_repo_viewer/app/presentation/home_page/widget/repo_card.dart';
import 'package:flutter_repo_viewer/app/presentation/home_page/widget/sort_widget.dart';
import 'package:flutter_repo_viewer/app/presentation/repository_details_page/repository_details_page.dart';
import 'package:flutter_repo_viewer/di/dependency_injector.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';

import 'package:go_router/go_router.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class HomePage extends HookConsumerWidget {
  static String get path => "/homePage";
  static String get name => "/homePage";
  const HomePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final stateController = ref.read(homePageNotifierProvider.notifier);

    final homePageState = ref.watch(homePageNotifierProvider);

    final scrollController = useScrollController();

    final pageNumber = useState(0);

    dataFetch() {
      stateController.repoListGet(pageNumber: pageNumber.value.toString());
    }

    scrollListner() {
      if (scrollController.offset >=
              scrollController.position.maxScrollExtent &&
          !scrollController.position.outOfRange) {
        pageNumber.value++;
        //  api Call
        dataFetch();
      }
    }

    // Listner
    useEffect(() {
      pageNumber.value = 1;
      Future.microtask(
          () => scrollController.addListener(() => scrollListner()));
      Future.microtask(() async =>
          await Future.delayed(Duration(seconds: 1), () => dataFetch()));

      return () {};
    }, []);

    ref.listen<SearchSortWordNotifierState>(searchSortWordNotifierProvider,
        (previous, next) {
      if ((previous!.isToggled != null) &&
          (previous.isToggled != next.isToggled)) {
        pageNumber.value = 1;
        stateController.init();
        dataFetch();
      }
    });

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          title: Text(
            'Home Page',
          ),
          centerTitle: true,
        ),
        backgroundColor: Color(0xFF151B22),
        body: Stack(
          children: [
            Container(
              height: 1.sh,
              width: 1.sw,
              padding: EdgeInsets.symmetric(horizontal: 16.w),
              child: homePageState.isAnyError
                  ? RetryPage(
                      onTry: () => dataFetch(),
                      failure: homePageState.failure!,
                    )
                  : Column(
                      children: [
                        SortWidget(),
                        Gap(6),
                        Expanded(
                          child: ListView.builder(
                            controller: scrollController,
                            itemCount: homePageState.gitRepoList?.length ?? 0,

                            // scrollDirection: Axis.vertical,
                            // physics: BouncingScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (_, i) => InkWell(
                              onTap: () => context.pushNamed(
                                  RepositoryDetailsPage.path,
                                  extra: homePageState.gitRepoList?[i]),
                              child: GitRepoCard(
                                repoName:
                                    homePageState.gitRepoList?[i].name ?? "",
                                repoDetails:
                                    homePageState.gitRepoList?[i].description ??
                                        "",
                                repoOwnerPhoto: homePageState
                                        .gitRepoList?[i].owner!.avatarUrl ??
                                    "",
                                repoStarCount: (homePageState
                                            .gitRepoList?[i].stargazersCount ??
                                        "")
                                    .toString(),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
            ),
            if (homePageState.isLoading != null && homePageState.isLoading!)
              Container(
                margin: EdgeInsets.symmetric(vertical: 24.h),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CustomText(
                        'Loading...',
                        fontColor: Colors.green,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 5.w),
                        height: 15.w,
                        width: 15.w,
                        child: CircularProgressIndicator(
                          color: Colors.white,
                          strokeWidth: 1.5,
                        ),
                      )
                    ],
                  ),
                ),
              )
          ],
        ),
      ),
    );
  }
}
