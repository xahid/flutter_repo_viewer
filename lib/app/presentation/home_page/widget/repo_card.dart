import 'package:flutter/material.dart';
import 'package:flutter_repo_viewer/app/core/widgets/network_image_handler.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:glassmorphism/glassmorphism.dart';
import 'package:lottie/lottie.dart';

class GitRepoCard extends StatelessWidget {
  final String repoName;
  final String repoDetails;
  final String repoStarCount;
  final String repoOwnerPhoto;

  const GitRepoCard({
    super.key,
    required this.repoName,
    required this.repoDetails,
    required this.repoStarCount,
    required this.repoOwnerPhoto,
  });

  @override
  Widget build(BuildContext context) {
    return GlassmorphicContainer(
      margin: EdgeInsets.symmetric(
        vertical: 8.h,
      ),
      width: double.infinity,
      height: 120.h,
      borderRadius: 20.r,
      // padding: EdgeInsets.symmetric(vertical: 10.w),
      blur: 2,
      alignment: Alignment.bottomCenter,
      border: 2,
      linearGradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color(0xFFffffff).withOpacity(0.1),
            Color(0xFFFFFFFF).withOpacity(0.09),
          ],
          stops: [
            0.1,
            1,
          ]),
      borderGradient: LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [
          Color(0xFFffffff).withOpacity(0.5),
          Color((0xFFFFFFFF)).withOpacity(0.5),
        ],
      ),
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 6.h,
        ),
        height: double.infinity,
        child: Row(
          children: [
            Gap(16),
            Expanded(
              flex: 2,
              child: NetworkImageHandler(
                networkImageUrl: repoOwnerPhoto,
                borderRadius: 24.r,
              ),
            ),
            Gap(16),
            Expanded(
                flex: 8,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Gap(4),
                    Flexible(
                      child: Text(
                        repoName,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 32.sp,
                          fontWeight: FontWeight.bold,
                        ),
                        overflow: TextOverflow.fade,
                      ),
                    ),
                    Gap(2),
                    Flexible(
                      child: Text(
                        repoDetails,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12.sp,
                          fontWeight: FontWeight.bold,
                        ),
                        overflow: TextOverflow.fade,
                      ),
                    ),
                  ],
                )),
            Expanded(
              flex: 4,
              child: Column(
                children: [
                  Spacer(),
                  Container(
                    child: Lottie.asset(
                      'assets/star.json',
                      height: 50.h,
                      width: 50.h,
                    ),
                  ),
                  Text(
                    repoStarCount,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.sp,
                      fontWeight: FontWeight.bold,
                    ),
                    overflow: TextOverflow.fade,
                  ),
                  Spacer(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
