import 'package:flutter/material.dart';
import 'package:flutter_repo_viewer/app/core/widgets/custom_text.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';

class SortButton extends StatelessWidget {
  final bool isSelected;
  final IconData icon;
  final String buttonText;
  const SortButton(
      {super.key,
      required this.isSelected,
      required this.icon,
      required this.buttonText});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 6.w,
        horizontal: 6.w,
      ),
      decoration: BoxDecoration(
          color: isSelected ? Colors.white : Colors.transparent,
          borderRadius: BorderRadius.all(
            Radius.circular(5.r),
          ),
          border: Border.all(color: Colors.white, width: 1)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            icon,
            color: isSelected ? Colors.blueAccent : Colors.white,
          ),
          Gap(3),
          CustomText(
            buttonText,
            fontColor: isSelected ? Colors.black : Colors.white,
          )
        ],
      ),
    );
  }
}
