import 'package:flutter/material.dart';
import 'package:flutter_repo_viewer/app/presentation/home_page/widget/sort_button.dart';
import 'package:flutter_repo_viewer/di/dependency_injector.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class SortWidget extends ConsumerWidget {
  const SortWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final sortWordState = ref.watch(searchSortWordNotifierProvider);

    final sortWordController =
        ref.read(searchSortWordNotifierProvider.notifier);
    return Container(
      height: 40.h,
      padding: EdgeInsets.symmetric(horizontal: 6.w),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: InkWell(
              onTap: () =>
                  sortWordController.changeSearchSortWord(word: "stars"),
              child: SortButton(
                isSelected: sortWordState.searchSortWord == "stars",
                icon: Icons.star,
                buttonText: "Sort by Star",
              ),
            ),
          ),
          Gap(16),
          Expanded(
            flex: 1,
            child: InkWell(
              onTap: () =>
                  sortWordController.changeSearchSortWord(word: "updated"),
              child: SortButton(
                isSelected: sortWordState.searchSortWord != "stars",
                icon: Icons.date_range,
                buttonText: "Sort by Date",
              ),
            ),
          ),
        ],
      ),
    );
  }
}
