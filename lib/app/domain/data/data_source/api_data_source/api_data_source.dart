import 'package:dartz/dartz.dart';
import 'package:flutter_repo_viewer/app/core/error/failure.dart';
import 'package:flutter_repo_viewer/app/infrastructure/data/local_data_source_i/repo_res_model.dart';

abstract class ApiDataSource {
  Future<Either<Failure, List<GitRepoItem>>> getGitRepolist({
    required String pageNumber,
    required String sortOrder,
  });
}
