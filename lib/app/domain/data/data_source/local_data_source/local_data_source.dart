import 'package:dartz/dartz.dart';
import 'package:flutter_repo_viewer/app/core/error/failure.dart';
import 'package:flutter_repo_viewer/app/infrastructure/data/local_data_source_i/repo_res_model.dart';

abstract class LocalDataSource {
  Future<bool> initDb();

  Future<Map<dynamic, dynamic>> computeDataForList(String boxName);

  Future<Either<Failure, List<GitRepoItem>>> getGitRepolist({
    required String pageNumber,
  });

  Future<bool> saveGitRepoListToLocal(
      {required List<GitRepoItem> items, required String pageNumber});

  Future<bool> changeSortOrder({required String searchSortWord});

  Future<String> getCurrentSortWord();
}
