import 'package:dartz/dartz.dart';
import 'package:flutter_repo_viewer/app/core/error/failure.dart';
import 'package:flutter_repo_viewer/app/infrastructure/data/local_data_source_i/repo_res_model.dart';

abstract class DataDomain {
  Future<Either<Failure, List<GitRepoItem>>> getGitRepolist({
    required String pageNumber,
  });
}
