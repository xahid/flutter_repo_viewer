import 'dart:io';

/// Returns a map of basic header information.
// ignore_for_file: non_constant_identifier_names

Map<String, String> BASIC_HEADER() {
  // String _password = dotenv.env['API_PASS'];
  // String _username = dotenv.env['API_USERNAME'];

  return {
    HttpHeaders.acceptHeader: "application/json",
    HttpHeaders.contentTypeHeader: "application/json",

    ///
    /// FOR Bearer Header
    ///
    ///

    // HttpHeaders.authorizationHeader: "Bearer $accessToken",
  };
}
