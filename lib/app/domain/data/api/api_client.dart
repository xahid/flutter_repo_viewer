import 'package:dartz/dartz.dart';
import 'package:flutter_repo_viewer/app/infrastructure/data/local_data_source_i/repo_res_model.dart';

import '../../../core/error/failure.dart';

/// Abstract class representing an API client.
abstract class ApiClient {
  /// Retrieves a list of repositories.
  ///
  /// Returns a [Future] that will resolve to an [Either] type representing either a [Failure] or a [RepoList].
  /// The [body] parameter is a required named parameter of type [Map<String, dynamic>].
  Future<Either<Failure, GitRepoResponse>> getRepoResponse(
      {required Map<String, dynamic> body});
}
