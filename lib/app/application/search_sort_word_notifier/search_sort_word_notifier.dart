import 'package:flutter_repo_viewer/app/application/search_sort_word_notifier/search_sort_word_notifier_state.dart';
import 'package:flutter_repo_viewer/app/domain/data/data_source/local_data_source/local_data_source.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class SearchSortWordNotifier
    extends StateNotifier<SearchSortWordNotifierState> {
  final LocalDataSource localDataSource;
  SearchSortWordNotifier({required this.localDataSource})
      : super(SearchSortWordNotifierState(searchSortWord: 'stars'));

  init() async =>
      await localDataSource.getCurrentSortWord().then((value) => state =
          SearchSortWordNotifierState(searchSortWord: value, isToggled: true));

  changeSearchSortWord({required String word}) {
    final toggle = state.isToggled ?? false;
    localDataSource.changeSortOrder(searchSortWord: word);
    Future.microtask(() => state =
        SearchSortWordNotifierState(searchSortWord: word, isToggled: !toggle));
  }
}
