import 'package:equatable/equatable.dart';

class SearchSortWordNotifierState extends Equatable {
  final String searchSortWord;
  final bool? isToggled;

  SearchSortWordNotifierState({this.isToggled, required this.searchSortWord});

  @override
  List<Object?> get props => [
        searchSortWord,
        isToggled,
      ];
}
