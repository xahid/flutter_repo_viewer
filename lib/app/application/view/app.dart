import 'package:flutter/material.dart';
import 'package:flutter_repo_viewer/app/core/route/route_provider.dart';
import 'package:flutter_repo_viewer/l10n/l10n.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class App extends ConsumerWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final router = ref.watch(routerProvider);

    return ScreenUtilInit(
      minTextAdapt: true,
      rebuildFactor: RebuildFactors.all,
      designSize: const Size(430, 896 - 36),
      builder: ((_, c) => MaterialApp.router(
            routeInformationParser: router.routeInformationParser,
            routeInformationProvider: router.routeInformationProvider,
            routerDelegate: router.routerDelegate,
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: AppLocalizations.supportedLocales,
            theme: ThemeData(
              textTheme: GoogleFonts.quicksandTextTheme(),
            ),
          )),
    );
  }
}
