import 'package:flutter_repo_viewer/app/application/home_page_notifier/home_page_notifier_state.dart';
import 'package:flutter_repo_viewer/app/domain/data/data_domain/data_domain.dart';
import 'package:flutter_repo_viewer/app/infrastructure/data/local_data_source_i/repo_res_model.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class HomePageNotifier extends StateNotifier<HomePageNotifierState> {
  final DataDomain dataDomain;
  HomePageNotifier({required this.dataDomain})
      : super(HomePageNotifierState.init());

  stateMaker(HomePageNotifierState newState) => state = newState;

  init() => stateMaker(HomePageNotifierState.init());

  repoListGet({required pageNumber}) async {
    stateMaker(state.copyWith(isLoading: true));
    await dataDomain.getGitRepolist(pageNumber: pageNumber).then((v) {
      v.fold((l) => stateMaker(state.copyWith(isAnyError: true, failure: l)),
          (r) {
        List<GitRepoItem>? temp = state.gitRepoList ?? [];
        temp.addAll(r);
        stateMaker(state.copyWith(
          isAnyError: false,
          isLoading: false,
          gitRepoList: temp,
        ));
      });
    });
    stateMaker(state.copyWith(isLoading: false));
  }
}
