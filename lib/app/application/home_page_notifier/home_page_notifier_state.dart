import 'package:equatable/equatable.dart';
import 'package:flutter_repo_viewer/app/core/error/failure.dart';
import 'package:flutter_repo_viewer/app/infrastructure/data/local_data_source_i/repo_res_model.dart';

class HomePageNotifierState extends Equatable {
  final bool? isLoading;
  final bool isAnyError;

  final List<GitRepoItem>? gitRepoList;
  final Failure? failure;
  const HomePageNotifierState({
    this.isLoading,
    this.gitRepoList,
    this.failure,
    required this.isAnyError,
  });

  @override
  List<Object?> get props => [isLoading, gitRepoList, failure, isAnyError];

  HomePageNotifierState copyWith({
    bool? isLoading,
    List<GitRepoItem>? gitRepoList,
    Failure? failure,
    bool? isAnyError,
  }) {
    return HomePageNotifierState(
      isLoading: isLoading ?? this.isLoading,
      gitRepoList: gitRepoList ?? this.gitRepoList,
      failure: failure ?? this.failure,
      isAnyError: isAnyError ?? this.isAnyError,
    );
  }

  factory HomePageNotifierState.init() => HomePageNotifierState(
        isLoading: false,
        isAnyError: false,
      );
}
