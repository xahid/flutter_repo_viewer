import 'package:flutter/material.dart';
import 'package:flutter_repo_viewer/app/core/error/failure.dart';
import 'package:flutter_repo_viewer/app/core/widgets/custom_text.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';

class RetryPage extends StatelessWidget {
  final VoidCallback onTry;
  final Failure failure;
  const RetryPage({
    super.key,
    required this.onTry,
    required this.failure,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFF151B22),
        body: SizedBox(
            height: 1.sh,
            width: 1.sw,
            child: Column(children: [
              Spacer(),
              GestureDetector(
                onTap: () => onTry(),
                child: Container(
                  height: 80.w,
                  width: 80.w,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                  child: Icon(
                    Icons.replay,
                    color: Colors.black,
                  ),
                ),
              ),
              Gap(16),
              CustomText('Retry'),
              Spacer(),
              CustomText(
                failure.message,
                fontColor: Colors.amber,
              ),
              if (failure.errLog != null)
                CustomText(
                  failure.errLog!,
                  fontColor: Colors.red,
                ),
              Gap(16),
            ])),
      ),
    );
  }
}
