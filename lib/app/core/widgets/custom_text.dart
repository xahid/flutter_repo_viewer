import 'package:flutter/material.dart';

class CustomText extends StatelessWidget {
  const CustomText(
    this.text, {
    this.fontSize,
    this.fontColor,
    this.fontWeight,
  });
  final String text;
  final double? fontSize;
  final Color? fontColor;
  final FontWeight? fontWeight;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontSize: fontSize,
        color: fontColor ?? Colors.white,
        fontWeight: fontWeight,
      ),
    );
  }
}
