import 'package:flutter/material.dart';

import '../values/asset_url.dart';

class NetworkImageHandler extends StatelessWidget {
  final String networkImageUrl;
  final double borderRadius;
  const NetworkImageHandler({
    super.key,
    required this.networkImageUrl,
    required this.borderRadius,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(borderRadius),
      child: FadeInImage.assetNetwork(
        placeholder: NETWORK_IMAGE_PLACE_HOLDER,
        image: networkImageUrl,
        imageErrorBuilder: (context, obj, error) => Image.asset(
          NETWORK_IMAGE_FALLBACK_IMAGE,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
