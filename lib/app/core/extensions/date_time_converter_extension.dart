import 'package:intl/intl.dart';

extension StringDateTimeExtension on String? {
  String formatDateString() {
    if (this == null) {
      return '';
    }

    final parsedDateTime = DateTime.tryParse(this!);
    if (parsedDateTime == null) {
      return '';
    }

    final formattedDate = DateFormat('MM-dd-yy HH:mm').format(parsedDateTime);
    return formattedDate;
  }
}
