import 'package:flutter/material.dart';
import 'package:flutter_repo_viewer/app/infrastructure/data/local_data_source_i/repo_res_model.dart';
import 'package:flutter_repo_viewer/app/presentation/home_page/home_page.dart';
import 'package:flutter_repo_viewer/app/presentation/repository_details_page/repository_details_page.dart';
import 'package:go_router/go_router.dart';

import 'error_route.dart';

class RouterNotifier extends ChangeNotifier {
  List<GoRoute> get routes => [
        GoRoute(
            path: HomePage.path,
            name: HomePage.name,
            builder: (context, state) => HomePage(),
            routes: [
              GoRoute(
                path: RepositoryDetailsPage.path,
                name: RepositoryDetailsPage.name,
                builder: (context, state) {
                  return RepositoryDetailsPage(
                    repoItem: state.extra as GitRepoItem,
                  );
                },
              )
            ])
      ];

  Page<void> errorPageBuilder(BuildContext context, GoRouterState state) =>
      MaterialPage(
        key: state.pageKey,
        child: Scaffold(
          // backgroundColor: Theme.of(context).errorColor.withOpacity(.1),
          body: ErrorPage(
            state: state,
          ),
        ),
      );
}
