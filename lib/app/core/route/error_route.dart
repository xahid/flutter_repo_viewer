import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class ErrorPage extends StatelessWidget {
  final GoRouterState state;
  const ErrorPage({
    Key? key,
    required this.state,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        //color Changed Here
        title: Text(
          "Something went wrong!",
          style: TextStyle(
            color: Colors.white,
          ),
        ),

        leading: null,
        centerTitle: true,
        elevation: 0,
      ),
      body: Column(
        children: [
          Spacer(),
          Center(
            child: Padding(
              padding: EdgeInsets.all(25),
              child: Text(
                'Error log: ${state.error.toString()}',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
            ),
          ),
          Spacer(),
        ],
      ),
    );
  }
}
