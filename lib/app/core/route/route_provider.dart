import 'package:flutter_repo_viewer/app/presentation/home_page/home_page.dart';
import 'package:flutter_repo_viewer/di/dependency_injector.dart';

import 'package:go_router/go_router.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final routerProvider = Provider<GoRouter>((ref) {
  final router = ref.watch(routerNotifierProvider);
  return GoRouter(
      debugLogDiagnostics: true,
      refreshListenable: router,
      routes: router.routes,
      initialLocation: HomePage.path,
      errorPageBuilder: router.errorPageBuilder,
      redirect: (context, state) {
        /// If there any login logic, will go here!
        return null;
        // return null;
      });
});
