class Failure {
  const Failure({
    required this.message,
    this.errLog,
  });
  final String message;
  final String? errLog;
}
