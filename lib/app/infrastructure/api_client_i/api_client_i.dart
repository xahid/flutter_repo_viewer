import 'dart:convert';
import 'package:dartz/dartz.dart';
import 'package:flutter_repo_viewer/app/domain/data/api/api_client.dart';
import 'package:flutter_repo_viewer/app/domain/data/api/api_header.dart';
import 'package:flutter_repo_viewer/app/core/error/failure.dart';
import 'package:flutter_repo_viewer/app/infrastructure/data/local_data_source_i/repo_res_model.dart';
import 'package:flutter_repo_viewer/flavor/build_config.dart';
import "package:http/http.dart" as http;

String _errString = "Something went wrong!";

/// Concrete implementation of the [ApiClient] abstract class.
class ApiClientImplementation extends ApiClient {
  @override
  Future<Either<Failure, GitRepoResponse>> getRepoResponse(
      {required Map<String, dynamic> body}) async {
    try {
      final response = await http
          .get(
            Uri.parse(
                    BuildConfig.instance.config.baseUrl + 'search/repositories')
                .replace(
              queryParameters: body,
            ),
            headers: BASIC_HEADER(),
          )
          .timeout(Duration(seconds: 60));

      if (response.statusCode == 200) {
        print("OK");
        GitRepoResponse repoResponse =
            GitRepoResponse.fromJson(jsonDecode(response.body));
        return Right(repoResponse);
      } else {
        print("Not OK || STATUS CODE [ ${response.statusCode} ]");
        Left(Failure(
          message: _errString,
          errLog: '${response.statusCode}',
        ));
      }
    } catch (e) {
      print("Not OK => $e");
      Left(
        Failure(
          message: _errString,
          errLog: e.toString(),
        ),
      );
    }

    return Left(Failure(message: _errString));
  }
}
