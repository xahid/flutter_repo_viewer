import 'package:dartz/dartz.dart';
import 'package:flutter_repo_viewer/app/core/error/failure.dart';
import 'package:flutter_repo_viewer/app/domain/data/data_domain/data_domain.dart';
import 'package:flutter_repo_viewer/app/domain/data/data_source/local_data_source/local_data_source.dart';
import 'package:flutter_repo_viewer/app/infrastructure/data/local_data_source_i/repo_res_model.dart';

class DataDomainImpl extends DataDomain {
  final LocalDataSource localDataSource;

  DataDomainImpl({required this.localDataSource});

  @override
  Future<Either<Failure, List<GitRepoItem>>> getGitRepolist({
    required String pageNumber,
  }) {
    return localDataSource.getGitRepolist(pageNumber: pageNumber);
  }
}
