import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_repo_viewer/app/core/error/failure.dart';
import 'package:flutter_repo_viewer/app/core/utils/toaster.dart';
import 'package:flutter_repo_viewer/app/domain/data/data_source/api_data_source/api_data_source.dart';
import 'package:flutter_repo_viewer/app/domain/data/data_source/local_data_source/local_data_source.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:flutter_repo_viewer/app/infrastructure/data/local_data_source_i/repo_res_model.dart';

import 'package:flutter_repo_viewer/flavor/build_config.dart';
import 'package:hive/hive.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:path_provider/path_provider.dart';

class LocalDataSourceImp extends LocalDataSource {
  RootIsolateToken rootIsolateToken = RootIsolateToken.instance!;
  final ApiDataSource apiDataSource;

  final String _sortLocalDataKey = "sortKey";

  LocalDataSourceImp({required this.apiDataSource});
  @override
  Future<bool> initDb() async {
    try {
      if (!foundation.kIsWeb) {
        final appDocumentDir = await getApplicationDocumentsDirectory();
        Hive.init(appDocumentDir.path);
      }

      Hive.registerAdapter(GitRepoResponseAdapter());
      Hive.registerAdapter(GitRepoItemAdapter());
      Hive.registerAdapter(OwnerAdapter());
      Hive.registerAdapter(LicenseAdapter());

      // For Storing Data.
      await Hive.openBox(BuildConfig.instance.config.hiveBoxName);

      return true;
    } on Exception catch (e, err) {
      print("DB error : $e");
      print(err);

      return false;
    }
  }

  @override
  Future<Map<dynamic, dynamic>> computeDataForList(String boxName) async {
    BackgroundIsolateBinaryMessenger.ensureInitialized(rootIsolateToken);
    if (!foundation.kIsWeb) {
      final appDocumentDir = await getApplicationDocumentsDirectory();
      Hive.init(appDocumentDir.path);
    }

    Hive.registerAdapter(GitRepoResponseAdapter());
    Hive.registerAdapter(GitRepoItemAdapter());
    Hive.registerAdapter(OwnerAdapter());
    Hive.registerAdapter(LicenseAdapter());
    await Hive.openBox(boxName);
    final box = Hive.box(boxName);
    return await box.get(boxName + '_repoListBox') ?? {};
  }

  @override
  Future<Either<Failure, List<GitRepoItem>>> getGitRepolist({
    required String pageNumber,
  }) async {
    WidgetsFlutterBinding.ensureInitialized();
    bool hasInterNet = await InternetConnectionChecker().hasConnection;

    if (!hasInterNet) {
      Toaster.error("App is in offline");
    }

    try {
      WidgetsFlutterBinding.ensureInitialized();
      Map<dynamic, dynamic> existingMap = await compute(
          computeDataForList, BuildConfig.instance.config.hiveBoxName);

      print("page number  : $pageNumber");

      final box = Hive.box(BuildConfig.instance.config.hiveBoxName);

      final String sortOrder = box.get(_sortLocalDataKey) ?? "stars";

      // Check if the repository with the given ID exists in the local storage

      if (existingMap.containsKey(pageNumber)) {
        List<GitRepoItem> repoList =
            List<GitRepoItem>.from(existingMap[pageNumber]!);
        return Right(repoList);
      } else {
        if (hasInterNet) {
          return apiDataSource
              .getGitRepolist(
            pageNumber: pageNumber,
            sortOrder: sortOrder,
          )
              .then((value) {
            value.fold(
                (l) => null,
                (r) =>
                    saveGitRepoListToLocal(items: r, pageNumber: pageNumber));
            return value;
          });
        } else {
          return Left(Failure(message: 'No Internet Connection'));
        }
      }
    } catch (e) {
      print("Error : $e");

      return Left(Failure(message: 'Something went Wrong', errLog: '$e'));
    }
  }

  @override
  Future<bool> saveGitRepoListToLocal(
      {required List<GitRepoItem> items, required String pageNumber}) async {
    try {
      final box = Hive.box(BuildConfig.instance.config.hiveBoxName);

      Map<dynamic, dynamic> newEntry = {pageNumber: items};

      Map<dynamic, dynamic> existingMap = await compute(
          computeDataForList, BuildConfig.instance.config.hiveBoxName);

      existingMap.addAll(newEntry);

      box.put(BuildConfig.instance.config.hiveBoxName + '_repoListBox',
          existingMap);
      print(box.values);
      return true;
    } catch (e) {
      print("Error : $e");
      return false;
    }
  }

  @override
  Future<bool> changeSortOrder({required String searchSortWord}) async {
    final box = Hive.box(BuildConfig.instance.config.hiveBoxName);
    final v = await box.clear();
    box.put(_sortLocalDataKey, searchSortWord);

    return v == 1;
  }

  @override
  Future<String> getCurrentSortWord() async {
    await initDb();

    final box = Hive.box(BuildConfig.instance.config.hiveBoxName);
    final currentSortWord = box.get(_sortLocalDataKey) ?? "stars";
    return currentSortWord;
  }
}
