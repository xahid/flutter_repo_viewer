import 'package:hive/hive.dart';

part 'repo_res_model.g.dart';

@HiveType(typeId: 0)
class GitRepoResponse {
  @HiveField(0)
  int? totalCount;
  @HiveField(1)
  bool? incompleteResults;
  @HiveField(2)
  List<GitRepoItem>? items;

  GitRepoResponse({this.totalCount, this.incompleteResults, this.items});

  GitRepoResponse.fromJson(Map<String, dynamic> json) {
    totalCount = json['total_count'];
    incompleteResults = json['incomplete_results'];
    if (json['items'] != null) {
      items = <GitRepoItem>[];
      json['items'].forEach((v) {
        items!.add(new GitRepoItem.fromJson(v));
      });
    }
  }
}

@HiveType(typeId: 1)
class GitRepoItem {
  @HiveField(1)
  int? id;
  @HiveField(2)
  String? nodeId;
  @HiveField(82)
  String? name;
  @HiveField(3)
  String? fullName;
  @HiveField(4)
  bool? private;
  @HiveField(5)
  Owner? owner;
  @HiveField(6)
  String? htmlUrl;
  @HiveField(7)
  String? description;
  @HiveField(8)
  bool? fork;
  @HiveField(9)
  String? url;
  @HiveField(10)
  String? forksUrl;
  @HiveField(11)
  String? keysUrl;
  @HiveField(12)
  String? collaboratorsUrl;
  @HiveField(13)
  String? teamsUrl;
  @HiveField(14)
  String? hooksUrl;
  @HiveField(15)
  String? issueEventsUrl;
  @HiveField(16)
  String? eventsUrl;
  @HiveField(17)
  String? assigneesUrl;
  @HiveField(18)
  String? branchesUrl;
  @HiveField(19)
  String? tagsUrl;
  @HiveField(20)
  String? blobsUrl;
  @HiveField(21)
  String? gitTagsUrl;
  @HiveField(22)
  String? gitRefsUrl;
  @HiveField(23)
  String? treesUrl;
  @HiveField(24)
  String? statusesUrl;
  @HiveField(25)
  String? languagesUrl;
  @HiveField(26)
  String? stargazersUrl;
  @HiveField(27)
  String? contributorsUrl;
  @HiveField(28)
  String? subscribersUrl;
  @HiveField(29)
  String? subscriptionUrl;
  @HiveField(30)
  String? commitsUrl;
  @HiveField(31)
  String? gitCommitsUrl;
  @HiveField(33)
  String? commentsUrl;
  @HiveField(34)
  String? issueCommentUrl;
  @HiveField(35)
  String? contentsUrl;
  @HiveField(36)
  String? compareUrl;
  @HiveField(37)
  String? mergesUrl;
  @HiveField(38)
  String? archiveUrl;
  @HiveField(39)
  String? downloadsUrl;
  @HiveField(40)
  String? issuesUrl;
  @HiveField(41)
  String? pullsUrl;
  @HiveField(42)
  String? milestonesUrl;
  @HiveField(43)
  String? notificationsUrl;
  @HiveField(44)
  String? labelsUrl;
  @HiveField(45)
  String? releasesUrl;
  @HiveField(46)
  String? deploymentsUrl;
  @HiveField(47)
  String? createdAt;
  @HiveField(48)
  String? updatedAt;
  @HiveField(49)
  String? pushedAt;
  @HiveField(50)
  String? gitUrl;
  @HiveField(51)
  String? sshUrl;
  @HiveField(52)
  String? cloneUrl;
  @HiveField(53)
  String? svnUrl;
  @HiveField(54)
  String? homepage;
  @HiveField(55)
  dynamic size;
  @HiveField(56)
  dynamic stargazersCount;
  @HiveField(57)
  dynamic watchersCount;
  @HiveField(58)
  String? language;
  @HiveField(59)
  bool? hasIssues;
  @HiveField(60)
  bool? hasProjects;
  @HiveField(61)
  bool? hasDownloads;
  @HiveField(62)
  bool? hasWiki;
  @HiveField(63)
  bool? hasPages;
  @HiveField(64)
  bool? hasDiscussions;
  @HiveField(65)
  dynamic forksCount;
  @HiveField(66)
  String? mirrorUrl;
  @HiveField(67)
  bool? archived;
  @HiveField(68)
  bool? disabled;
  @HiveField(69)
  dynamic openIssuesCount;
  @HiveField(70)
  License? license;
  @HiveField(71)
  bool? allowForking;
  @HiveField(72)
  bool? isTemplate;
  @HiveField(73)
  bool? webCommitSignoffRequired;
  @HiveField(74)
  List<String>? topics;
  @HiveField(75)
  String? visibility;
  @HiveField(76)
  dynamic forks;
  @HiveField(77)
  dynamic openIssues;
  @HiveField(78)
  dynamic watchers;
  @HiveField(79)
  String? defaultBranch;
  @HiveField(80)
  dynamic score;
  @HiveField(81)
  GitRepoItem(
      {this.id,
      this.nodeId,
      this.name,
      this.fullName,
      this.private,
      this.owner,
      this.htmlUrl,
      this.description,
      this.fork,
      this.url,
      this.forksUrl,
      this.keysUrl,
      this.collaboratorsUrl,
      this.teamsUrl,
      this.hooksUrl,
      this.issueEventsUrl,
      this.eventsUrl,
      this.assigneesUrl,
      this.branchesUrl,
      this.tagsUrl,
      this.blobsUrl,
      this.gitTagsUrl,
      this.gitRefsUrl,
      this.treesUrl,
      this.statusesUrl,
      this.languagesUrl,
      this.stargazersUrl,
      this.contributorsUrl,
      this.subscribersUrl,
      this.subscriptionUrl,
      this.commitsUrl,
      this.gitCommitsUrl,
      this.commentsUrl,
      this.issueCommentUrl,
      this.contentsUrl,
      this.compareUrl,
      this.mergesUrl,
      this.archiveUrl,
      this.downloadsUrl,
      this.issuesUrl,
      this.pullsUrl,
      this.milestonesUrl,
      this.notificationsUrl,
      this.labelsUrl,
      this.releasesUrl,
      this.deploymentsUrl,
      this.createdAt,
      this.updatedAt,
      this.pushedAt,
      this.gitUrl,
      this.sshUrl,
      this.cloneUrl,
      this.svnUrl,
      this.homepage,
      this.size,
      this.stargazersCount,
      this.watchersCount,
      this.language,
      this.hasIssues,
      this.hasProjects,
      this.hasDownloads,
      this.hasWiki,
      this.hasPages,
      this.hasDiscussions,
      this.forksCount,
      this.mirrorUrl,
      this.archived,
      this.disabled,
      this.openIssuesCount,
      this.license,
      this.allowForking,
      this.isTemplate,
      this.webCommitSignoffRequired,
      this.topics,
      this.visibility,
      this.forks,
      this.openIssues,
      this.watchers,
      this.defaultBranch,
      this.score});

  GitRepoItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nodeId = json['node_id'];
    name = json['name'];
    fullName = json['full_name'];
    private = json['private'];
    owner = json['owner'] != null ? new Owner.fromJson(json['owner']) : null;
    htmlUrl = json['html_url'];
    description = json['description'];
    fork = json['fork'];
    url = json['url'];
    forksUrl = json['forks_url'];
    keysUrl = json['keys_url'];
    collaboratorsUrl = json['collaborators_url'];
    teamsUrl = json['teams_url'];
    hooksUrl = json['hooks_url'];
    issueEventsUrl = json['issue_events_url'];
    eventsUrl = json['events_url'];
    assigneesUrl = json['assignees_url'];
    branchesUrl = json['branches_url'];
    tagsUrl = json['tags_url'];
    blobsUrl = json['blobs_url'];
    gitTagsUrl = json['git_tags_url'];
    gitRefsUrl = json['git_refs_url'];
    treesUrl = json['trees_url'];
    statusesUrl = json['statuses_url'];
    languagesUrl = json['languages_url'];
    stargazersUrl = json['stargazers_url'];
    contributorsUrl = json['contributors_url'];
    subscribersUrl = json['subscribers_url'];
    subscriptionUrl = json['subscription_url'];
    commitsUrl = json['commits_url'];
    gitCommitsUrl = json['git_commits_url'];
    commentsUrl = json['comments_url'];
    issueCommentUrl = json['issue_comment_url'];
    contentsUrl = json['contents_url'];
    compareUrl = json['compare_url'];
    mergesUrl = json['merges_url'];
    archiveUrl = json['archive_url'];
    downloadsUrl = json['downloads_url'];
    issuesUrl = json['issues_url'];
    pullsUrl = json['pulls_url'];
    milestonesUrl = json['milestones_url'];
    notificationsUrl = json['notifications_url'];
    labelsUrl = json['labels_url'];
    releasesUrl = json['releases_url'];
    deploymentsUrl = json['deployments_url'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    pushedAt = json['pushed_at'];
    gitUrl = json['git_url'];
    sshUrl = json['ssh_url'];
    cloneUrl = json['clone_url'];
    svnUrl = json['svn_url'];
    homepage = json['homepage'];
    size = json['size'];
    stargazersCount = json['stargazers_count'];
    watchersCount = json['watchers_count'];
    language = json['language'];
    hasIssues = json['has_issues'];
    hasProjects = json['has_projects'];
    hasDownloads = json['has_downloads'];
    hasWiki = json['has_wiki'];
    hasPages = json['has_pages'];
    hasDiscussions = json['has_discussions'];
    forksCount = json['forks_count'];
    mirrorUrl = json['mirror_url'];
    archived = json['archived'];
    disabled = json['disabled'];
    openIssuesCount = json['open_issues_count'];
    license =
        json['license'] != null ? new License.fromJson(json['license']) : null;
    allowForking = json['allow_forking'];
    isTemplate = json['is_template'];
    webCommitSignoffRequired = json['web_commit_signoff_required'];
    topics = json['topics'] == null ? null : json['topics'].cast<String>();
    visibility = json['visibility'];
    forks = json['forks'];
    openIssues = json['open_issues'];
    watchers = json['watchers'];
    defaultBranch = json['default_branch'];
    score = json['score'];
  }
}

@HiveType(typeId: 3)
class Owner {
  @HiveField(0)
  String? login;
  @HiveField(1)
  int? id;
  @HiveField(2)
  String? nodeId;
  @HiveField(3)
  String? avatarUrl;
  @HiveField(4)
  String? gravatarId;
  @HiveField(5)
  String? url;
  @HiveField(6)
  String? htmlUrl;
  @HiveField(7)
  String? followersUrl;
  @HiveField(8)
  String? followingUrl;
  @HiveField(9)
  String? gistsUrl;
  @HiveField(10)
  String? starredUrl;
  @HiveField(11)
  String? subscriptionsUrl;
  @HiveField(12)
  String? organizationsUrl;
  @HiveField(13)
  String? reposUrl;
  @HiveField(14)
  String? eventsUrl;
  @HiveField(15)
  String? receivedEventsUrl;
  @HiveField(16)
  String? type;
  @HiveField(17)
  bool? siteAdmin;

  Owner(
      {this.login,
      this.id,
      this.nodeId,
      this.avatarUrl,
      this.gravatarId,
      this.url,
      this.htmlUrl,
      this.followersUrl,
      this.followingUrl,
      this.gistsUrl,
      this.starredUrl,
      this.subscriptionsUrl,
      this.organizationsUrl,
      this.reposUrl,
      this.eventsUrl,
      this.receivedEventsUrl,
      this.type,
      this.siteAdmin});

  Owner.fromJson(Map<String, dynamic> json) {
    login = json['login'];
    id = json['id'];
    nodeId = json['node_id'];
    avatarUrl = json['avatar_url'];
    gravatarId = json['gravatar_id'];
    url = json['url'];
    htmlUrl = json['html_url'];
    followersUrl = json['followers_url'];
    followingUrl = json['following_url'];
    gistsUrl = json['gists_url'];
    starredUrl = json['starred_url'];
    subscriptionsUrl = json['subscriptions_url'];
    organizationsUrl = json['organizations_url'];
    reposUrl = json['repos_url'];
    eventsUrl = json['events_url'];
    receivedEventsUrl = json['received_events_url'];
    type = json['type'];
    siteAdmin = json['site_admin'];
  }
}

@HiveType(typeId: 4)
class License {
  @HiveField(0)
  String? key;
  @HiveField(1)
  String? name;
  @HiveField(2)
  String? spdxId;
  @HiveField(3)
  String? url;
  @HiveField(4)
  String? nodeId;

  License({this.key, this.name, this.spdxId, this.url, this.nodeId});

  License.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    name = json['name'];
    spdxId = json['spdx_id'];
    url = json['url'];
    nodeId = json['node_id'];
  }
}
