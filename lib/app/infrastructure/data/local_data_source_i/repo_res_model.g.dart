// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'repo_res_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class GitRepoResponseAdapter extends TypeAdapter<GitRepoResponse> {
  @override
  final int typeId = 0;

  @override
  GitRepoResponse read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return GitRepoResponse(
      totalCount: fields[0] as int?,
      incompleteResults: fields[1] as bool?,
      items: (fields[2] as List?)?.cast<GitRepoItem>(),
    );
  }

  @override
  void write(BinaryWriter writer, GitRepoResponse obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.totalCount)
      ..writeByte(1)
      ..write(obj.incompleteResults)
      ..writeByte(2)
      ..write(obj.items);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is GitRepoResponseAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class GitRepoItemAdapter extends TypeAdapter<GitRepoItem> {
  @override
  final int typeId = 1;

  @override
  GitRepoItem read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return GitRepoItem(
      id: fields[1] as int?,
      nodeId: fields[2] as String?,
      name: fields[82] as String?,
      fullName: fields[3] as String?,
      private: fields[4] as bool?,
      owner: fields[5] as Owner?,
      htmlUrl: fields[6] as String?,
      description: fields[7] as String?,
      fork: fields[8] as bool?,
      url: fields[9] as String?,
      forksUrl: fields[10] as String?,
      keysUrl: fields[11] as String?,
      collaboratorsUrl: fields[12] as String?,
      teamsUrl: fields[13] as String?,
      hooksUrl: fields[14] as String?,
      issueEventsUrl: fields[15] as String?,
      eventsUrl: fields[16] as String?,
      assigneesUrl: fields[17] as String?,
      branchesUrl: fields[18] as String?,
      tagsUrl: fields[19] as String?,
      blobsUrl: fields[20] as String?,
      gitTagsUrl: fields[21] as String?,
      gitRefsUrl: fields[22] as String?,
      treesUrl: fields[23] as String?,
      statusesUrl: fields[24] as String?,
      languagesUrl: fields[25] as String?,
      stargazersUrl: fields[26] as String?,
      contributorsUrl: fields[27] as String?,
      subscribersUrl: fields[28] as String?,
      subscriptionUrl: fields[29] as String?,
      commitsUrl: fields[30] as String?,
      gitCommitsUrl: fields[31] as String?,
      commentsUrl: fields[33] as String?,
      issueCommentUrl: fields[34] as String?,
      contentsUrl: fields[35] as String?,
      compareUrl: fields[36] as String?,
      mergesUrl: fields[37] as String?,
      archiveUrl: fields[38] as String?,
      downloadsUrl: fields[39] as String?,
      issuesUrl: fields[40] as String?,
      pullsUrl: fields[41] as String?,
      milestonesUrl: fields[42] as String?,
      notificationsUrl: fields[43] as String?,
      labelsUrl: fields[44] as String?,
      releasesUrl: fields[45] as String?,
      deploymentsUrl: fields[46] as String?,
      createdAt: fields[47] as String?,
      updatedAt: fields[48] as String?,
      pushedAt: fields[49] as String?,
      gitUrl: fields[50] as String?,
      sshUrl: fields[51] as String?,
      cloneUrl: fields[52] as String?,
      svnUrl: fields[53] as String?,
      homepage: fields[54] as String?,
      size: fields[55] as dynamic,
      stargazersCount: fields[56] as dynamic,
      watchersCount: fields[57] as dynamic,
      language: fields[58] as String?,
      hasIssues: fields[59] as bool?,
      hasProjects: fields[60] as bool?,
      hasDownloads: fields[61] as bool?,
      hasWiki: fields[62] as bool?,
      hasPages: fields[63] as bool?,
      hasDiscussions: fields[64] as bool?,
      forksCount: fields[65] as dynamic,
      mirrorUrl: fields[66] as String?,
      archived: fields[67] as bool?,
      disabled: fields[68] as bool?,
      openIssuesCount: fields[69] as dynamic,
      license: fields[70] as License?,
      allowForking: fields[71] as bool?,
      isTemplate: fields[72] as bool?,
      webCommitSignoffRequired: fields[73] as bool?,
      topics: (fields[74] as List?)?.cast<String>(),
      visibility: fields[75] as String?,
      forks: fields[76] as dynamic,
      openIssues: fields[77] as dynamic,
      watchers: fields[78] as dynamic,
      defaultBranch: fields[79] as String?,
      score: fields[80] as dynamic,
    );
  }

  @override
  void write(BinaryWriter writer, GitRepoItem obj) {
    writer
      ..writeByte(80)
      ..writeByte(1)
      ..write(obj.id)
      ..writeByte(2)
      ..write(obj.nodeId)
      ..writeByte(82)
      ..write(obj.name)
      ..writeByte(3)
      ..write(obj.fullName)
      ..writeByte(4)
      ..write(obj.private)
      ..writeByte(5)
      ..write(obj.owner)
      ..writeByte(6)
      ..write(obj.htmlUrl)
      ..writeByte(7)
      ..write(obj.description)
      ..writeByte(8)
      ..write(obj.fork)
      ..writeByte(9)
      ..write(obj.url)
      ..writeByte(10)
      ..write(obj.forksUrl)
      ..writeByte(11)
      ..write(obj.keysUrl)
      ..writeByte(12)
      ..write(obj.collaboratorsUrl)
      ..writeByte(13)
      ..write(obj.teamsUrl)
      ..writeByte(14)
      ..write(obj.hooksUrl)
      ..writeByte(15)
      ..write(obj.issueEventsUrl)
      ..writeByte(16)
      ..write(obj.eventsUrl)
      ..writeByte(17)
      ..write(obj.assigneesUrl)
      ..writeByte(18)
      ..write(obj.branchesUrl)
      ..writeByte(19)
      ..write(obj.tagsUrl)
      ..writeByte(20)
      ..write(obj.blobsUrl)
      ..writeByte(21)
      ..write(obj.gitTagsUrl)
      ..writeByte(22)
      ..write(obj.gitRefsUrl)
      ..writeByte(23)
      ..write(obj.treesUrl)
      ..writeByte(24)
      ..write(obj.statusesUrl)
      ..writeByte(25)
      ..write(obj.languagesUrl)
      ..writeByte(26)
      ..write(obj.stargazersUrl)
      ..writeByte(27)
      ..write(obj.contributorsUrl)
      ..writeByte(28)
      ..write(obj.subscribersUrl)
      ..writeByte(29)
      ..write(obj.subscriptionUrl)
      ..writeByte(30)
      ..write(obj.commitsUrl)
      ..writeByte(31)
      ..write(obj.gitCommitsUrl)
      ..writeByte(33)
      ..write(obj.commentsUrl)
      ..writeByte(34)
      ..write(obj.issueCommentUrl)
      ..writeByte(35)
      ..write(obj.contentsUrl)
      ..writeByte(36)
      ..write(obj.compareUrl)
      ..writeByte(37)
      ..write(obj.mergesUrl)
      ..writeByte(38)
      ..write(obj.archiveUrl)
      ..writeByte(39)
      ..write(obj.downloadsUrl)
      ..writeByte(40)
      ..write(obj.issuesUrl)
      ..writeByte(41)
      ..write(obj.pullsUrl)
      ..writeByte(42)
      ..write(obj.milestonesUrl)
      ..writeByte(43)
      ..write(obj.notificationsUrl)
      ..writeByte(44)
      ..write(obj.labelsUrl)
      ..writeByte(45)
      ..write(obj.releasesUrl)
      ..writeByte(46)
      ..write(obj.deploymentsUrl)
      ..writeByte(47)
      ..write(obj.createdAt)
      ..writeByte(48)
      ..write(obj.updatedAt)
      ..writeByte(49)
      ..write(obj.pushedAt)
      ..writeByte(50)
      ..write(obj.gitUrl)
      ..writeByte(51)
      ..write(obj.sshUrl)
      ..writeByte(52)
      ..write(obj.cloneUrl)
      ..writeByte(53)
      ..write(obj.svnUrl)
      ..writeByte(54)
      ..write(obj.homepage)
      ..writeByte(55)
      ..write(obj.size)
      ..writeByte(56)
      ..write(obj.stargazersCount)
      ..writeByte(57)
      ..write(obj.watchersCount)
      ..writeByte(58)
      ..write(obj.language)
      ..writeByte(59)
      ..write(obj.hasIssues)
      ..writeByte(60)
      ..write(obj.hasProjects)
      ..writeByte(61)
      ..write(obj.hasDownloads)
      ..writeByte(62)
      ..write(obj.hasWiki)
      ..writeByte(63)
      ..write(obj.hasPages)
      ..writeByte(64)
      ..write(obj.hasDiscussions)
      ..writeByte(65)
      ..write(obj.forksCount)
      ..writeByte(66)
      ..write(obj.mirrorUrl)
      ..writeByte(67)
      ..write(obj.archived)
      ..writeByte(68)
      ..write(obj.disabled)
      ..writeByte(69)
      ..write(obj.openIssuesCount)
      ..writeByte(70)
      ..write(obj.license)
      ..writeByte(71)
      ..write(obj.allowForking)
      ..writeByte(72)
      ..write(obj.isTemplate)
      ..writeByte(73)
      ..write(obj.webCommitSignoffRequired)
      ..writeByte(74)
      ..write(obj.topics)
      ..writeByte(75)
      ..write(obj.visibility)
      ..writeByte(76)
      ..write(obj.forks)
      ..writeByte(77)
      ..write(obj.openIssues)
      ..writeByte(78)
      ..write(obj.watchers)
      ..writeByte(79)
      ..write(obj.defaultBranch)
      ..writeByte(80)
      ..write(obj.score);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is GitRepoItemAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class OwnerAdapter extends TypeAdapter<Owner> {
  @override
  final int typeId = 3;

  @override
  Owner read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Owner(
      login: fields[0] as String?,
      id: fields[1] as int?,
      nodeId: fields[2] as String?,
      avatarUrl: fields[3] as String?,
      gravatarId: fields[4] as String?,
      url: fields[5] as String?,
      htmlUrl: fields[6] as String?,
      followersUrl: fields[7] as String?,
      followingUrl: fields[8] as String?,
      gistsUrl: fields[9] as String?,
      starredUrl: fields[10] as String?,
      subscriptionsUrl: fields[11] as String?,
      organizationsUrl: fields[12] as String?,
      reposUrl: fields[13] as String?,
      eventsUrl: fields[14] as String?,
      receivedEventsUrl: fields[15] as String?,
      type: fields[16] as String?,
      siteAdmin: fields[17] as bool?,
    );
  }

  @override
  void write(BinaryWriter writer, Owner obj) {
    writer
      ..writeByte(18)
      ..writeByte(0)
      ..write(obj.login)
      ..writeByte(1)
      ..write(obj.id)
      ..writeByte(2)
      ..write(obj.nodeId)
      ..writeByte(3)
      ..write(obj.avatarUrl)
      ..writeByte(4)
      ..write(obj.gravatarId)
      ..writeByte(5)
      ..write(obj.url)
      ..writeByte(6)
      ..write(obj.htmlUrl)
      ..writeByte(7)
      ..write(obj.followersUrl)
      ..writeByte(8)
      ..write(obj.followingUrl)
      ..writeByte(9)
      ..write(obj.gistsUrl)
      ..writeByte(10)
      ..write(obj.starredUrl)
      ..writeByte(11)
      ..write(obj.subscriptionsUrl)
      ..writeByte(12)
      ..write(obj.organizationsUrl)
      ..writeByte(13)
      ..write(obj.reposUrl)
      ..writeByte(14)
      ..write(obj.eventsUrl)
      ..writeByte(15)
      ..write(obj.receivedEventsUrl)
      ..writeByte(16)
      ..write(obj.type)
      ..writeByte(17)
      ..write(obj.siteAdmin);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is OwnerAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class LicenseAdapter extends TypeAdapter<License> {
  @override
  final int typeId = 4;

  @override
  License read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return License(
      key: fields[0] as String?,
      name: fields[1] as String?,
      spdxId: fields[2] as String?,
      url: fields[3] as String?,
      nodeId: fields[4] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, License obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.key)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.spdxId)
      ..writeByte(3)
      ..write(obj.url)
      ..writeByte(4)
      ..write(obj.nodeId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LicenseAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
