import 'package:dartz/dartz.dart';
import 'package:flutter_repo_viewer/app/core/error/failure.dart';
import 'package:flutter_repo_viewer/app/domain/data/api/api_client.dart';
import 'package:flutter_repo_viewer/app/domain/data/data_source/api_data_source/api_data_source.dart';
import 'package:flutter_repo_viewer/app/infrastructure/data/local_data_source_i/repo_res_model.dart';

class ApiDataSourceImpl extends ApiDataSource {
  final ApiClient apiClient;
  // final LocalDataSource localDataSource;

  ApiDataSourceImpl({
    required this.apiClient,
    // required this.localDataSource,
  });

  @override
  Future<Either<Failure, List<GitRepoItem>>> getGitRepolist({
    required String pageNumber,
    required String sortOrder,
  }) async {
    Map<String, dynamic> body = {
      "q": "flutter",
      "sort": sortOrder,
      "order": "desc",
      "per_page": "10",
      "page": "$pageNumber",
    };
    return await apiClient
        .getRepoResponse(body: body)
        .then((v) => v.fold((failure) => Left(failure), (gitRepoResponse) {
              return Right(gitRepoResponse.items!);
            }));
  }
}
