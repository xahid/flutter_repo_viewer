import 'package:flutter/material.dart';
import 'package:flutter_repo_viewer/di/dependency_injector.dart' as _di;
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'app/application/view/app.dart';
import 'flavor/build_config.dart';
import 'flavor/env_config.dart';
import 'flavor/enviroment.dart';

void main() async {
  EnvConfig devConfig = EnvConfig(
    appName: "Flutter Repo viewer-Staging",
    shouldCollectCrashLog: true,
    // baseUrl: dotenv.env['BASEURL_STAGING'],
    baseUrl: 'https://api.github.com/',
    hiveBoxName: 'hive_box_stage',
  );

  BuildConfig.instantiate(
    envType: Environment.STAGING,
    envConfig: devConfig,
  );
  WidgetsFlutterBinding.ensureInitialized();
  await _di.init();

  runApp(ProviderScope(
    child: App(),
  ));
}
