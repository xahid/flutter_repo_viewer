import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'app/application/view/app.dart';
import 'flavor/build_config.dart';
import 'flavor/env_config.dart';
import 'flavor/enviroment.dart';
import 'package:flutter_repo_viewer/di/dependency_injector.dart' as _di;

void main() async {
  EnvConfig devConfig = EnvConfig(
    appName: "Flutter Repo viewer",
    shouldCollectCrashLog: true,
    // baseUrl: dotenv.env['BASEURL_PRODUCTION'],
    baseUrl: 'https://api.github.com/',
    hiveBoxName: 'hive_box_prod',
  );

  BuildConfig.instantiate(
    envType: Environment.PRODUCTION,
    envConfig: devConfig,
  );
  WidgetsFlutterBinding.ensureInitialized();
  await _di.init();
  runApp(ProviderScope(
    child: App(),
  ));
}
