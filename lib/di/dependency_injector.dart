import 'package:flutter_repo_viewer/app/application/home_page_notifier/home_page_notifier.dart';
import 'package:flutter_repo_viewer/app/application/home_page_notifier/home_page_notifier_state.dart';
import 'package:flutter_repo_viewer/app/application/search_sort_word_notifier/search_sort_word_notifier.dart';
import 'package:flutter_repo_viewer/app/application/search_sort_word_notifier/search_sort_word_notifier_state.dart';
import 'package:flutter_repo_viewer/app/core/route/routes.dart';
import 'package:flutter_repo_viewer/app/domain/data/api/api_client.dart';
import 'package:flutter_repo_viewer/app/domain/data/data_domain/data_domain.dart';
import 'package:flutter_repo_viewer/app/domain/data/data_source/local_data_source/local_data_source.dart';
import 'package:flutter_repo_viewer/app/infrastructure/api_client_i/api_client_i.dart';
import 'package:flutter_repo_viewer/app/infrastructure/data/api_data_source_i/api_data_source_i.dart';
import 'package:flutter_repo_viewer/app/infrastructure/data/local_data_source_i/local_data_source_i.dart';
import 'package:flutter_repo_viewer/app/infrastructure/data_domain_i/data_domain_i.dart';
import 'package:get_it/get_it.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../app/domain/data/data_source/api_data_source/api_data_source.dart';

final sl = GetIt.instance;

Future<void> init() async {
  sl.registerLazySingleton<ApiClient>(() => ApiClientImplementation());
  //
  sl.registerLazySingleton<ApiDataSource>(() => ApiDataSourceImpl(
        apiClient: sl<ApiClient>(),
      ));

  sl.registerLazySingleton<LocalDataSource>(
      () => LocalDataSourceImp(apiDataSource: sl<ApiDataSource>()));

  sl.registerLazySingleton<DataDomain>(
      () => DataDomainImpl(localDataSource: sl<LocalDataSource>()));
}

// Providers

// Router Notifire provider
final routerNotifierProvider = Provider<RouterNotifier>((ref) {
  return RouterNotifier();
});

final homePageNotifierProvider =
    StateNotifierProvider<HomePageNotifier, HomePageNotifierState>((ref) {
  return HomePageNotifier(dataDomain: sl());
});

final searchSortWordNotifierProvider =
    StateNotifierProvider<SearchSortWordNotifier, SearchSortWordNotifierState>(
        (ref) {
  return SearchSortWordNotifier(localDataSource: sl())..init();
});
